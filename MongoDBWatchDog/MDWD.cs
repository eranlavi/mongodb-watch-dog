﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Reflection;
using System.IO;
using System.Configuration;

namespace MongoDBWatchDog
{
    public partial class MDWD : ServiceBase
    {
        System.Timers.Timer tmr;
        private string src = ConfigurationManager.AppSettings["ServiceName"];
        Object _LogLocker = new Object();
        string LogFile = string.Empty;

        public MDWD()
        {
            InitializeComponent();

        }

        protected override void OnStart(string[] args)
        {
            String assemblyLocationFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            if (String.Compare(Environment.CurrentDirectory, assemblyLocationFolder, StringComparison.OrdinalIgnoreCase) != 0)
            {
                Environment.CurrentDirectory = assemblyLocationFolder;
            }

            DirectoryInfo dir = new DirectoryInfo(System.Configuration.ConfigurationManager.AppSettings["LogFile"]);
            if (!dir.Parent.Exists)
                dir.Parent.Create();

            LogFile = ConfigurationManager.AppSettings["LogFile"].Replace(".txt", "_" + string.Format("{0:dd/MM/yyyy}", DateTime.Now).Replace("/", "").Replace("\\", "") + ".txt");

            Log("Service Started");
            Log("Check interval is " + ConfigurationManager.AppSettings["CheckInterval"] + " msec");
            Log("Lock file location: " + ConfigurationManager.AppSettings["LockFilePath"]);

            tmr = new System.Timers.Timer(Convert.ToInt32(ConfigurationManager.AppSettings["CheckInterval"]));
            tmr.Elapsed += new System.Timers.ElapsedEventHandler(tmr_Elapsed);
            tmr.Enabled = false;

            if (!CheckServiceStatus())
                tmr.Enabled = true;
        }

        void tmr_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            tmr.Enabled = false;

            if (CheckServiceStatus())
                return;
                       
            tmr.Enabled = true;
        }

        protected override void OnStop()
        {
            tmr.Enabled = false;
            try
            {
                tmr.Dispose();
            }
            catch { }

            Log("Service Stopped");
        }

        private bool CheckServiceStatus()
        {
            try
            {
                ServiceController[] scServices;
                scServices = ServiceController.GetServices();

                foreach (ServiceController scTemp in scServices)
                {
                    if (scTemp.ServiceName == src)
                    {
                        using (ServiceController sc = new ServiceController())
                        {
                            sc.DisplayName = ConfigurationManager.AppSettings["ServiceDisplayName"];
                            if (sc.Status != ServiceControllerStatus.Running)
                            {
                                try
                                {
                                    System.IO.File.Delete(ConfigurationManager.AppSettings["LockFilePath"]);
                                }
                                catch (Exception ex)
                                {
                                    Log("Could not delete " + ConfigurationManager.AppSettings["LockFilePath"] + ". " + ex.Message);
                                }
                                return false;
                            }
                            else
                            {
                                Log(sc.DisplayName + " service is in Run state. No further actions will be taken.");
                                return true;
                            }
                        }                        
                    }
                }

                Log("Could not find service: " + src);

                return true;
            }
            catch (Exception ex)
            {
                Log("Exception: " + ex.Message);                
            }

            return false;

        }

        void Log(string data)
        {
            lock (_LogLocker)
            {
                try
                {
                    string log = ConfigurationManager.AppSettings["LogFile"].Replace(".txt", "_" + string.Format("{0:dd/MM/yyyy}", DateTime.Now).Replace("/", "").Replace("\\", "") + ".txt");
                    if (log != LogFile)
                        LogFile = log;

                    string date = string.Format("{0:MM/dd/yyyy H:mm:ss:fff} ", DateTime.Now);

                    try
                    {

                        System.IO.File.AppendAllText(LogFile, date + data + "\r\n");
                    }
                    catch { }
                }
                catch { }
            }
        }



    }
}
